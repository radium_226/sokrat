define(['./module'], function (directives) {
    'use strict';
    directives.directive('autocomplete', function () {
        return {
            restrict: 'AEC',
            replace: true,
            controller: ['$scope', '$http', function ($scope, $http) {
                $scope.editMod = function (m,e) {
                    e.stopPropagation();
                    $scope.mod.text = m.text;
                    $scope.mod.value = m.value;
                };
                $scope.$watch("mod.text", function (newValue) {
                    if(newValue!==undefined && newValue)
                    $http.get($scope.url,{params:{text:newValue.trim()}}).then(function (response) {
                        $scope.list = response.data;
                    });
                });
            }],
            scope: {
                url: "=",
                mod: "="
            },
            templateUrl: '/templates/directives/autoComplete.html'
        }
    });
});
