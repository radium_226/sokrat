/**
 * Created by emmtech on 09.06.16.
 */
define(['./module','jquery'],function(controllers,$) {
    'use strict';

    controllers.controller('Subject', ['$stateParams','ngSocket','$scope','$state', function ($stateParams,ngSocket, $scope, $state) {
        let offset = parseInt($stateParams.offset==null?0:$stateParams.offset);
        let limit = 4;
        $scope.sortParams = $scope.$stateParams.sort ? JSON.parse($scope.$stateParams.sort) : {};
        $scope.sendSearchRequest = $scope.$stateParams.filter ? JSON.parse($scope.$stateParams.filter) : {};

        $scope.sendRequest = function () {
            ngSocket.emit('getScheduleList', {
                limit: limit,
                offset: offset,
                sort: $scope.sortParams,
                filter: $scope.sendSearchRequest
            });
        };

        $scope.$on('$viewContentLoading', function () {
            if ($scope.$state.current.name === 'subjects') {
                $scope.sendRequest();
              }
            });


        $scope.sendFilter = function (e) {
            if (e.keyCode === 13) {
                $scope.goToState();
            }
        };

        $scope.goToState = function () {
            offset = arguments.length ? arguments[0] : offset;
            $state.go($state.current.name, {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset
            });
        };

        $scope.goPreviousPage = function() {
            if ($stateParams.offset == 0 || $stateParams.offset == undefined) return;
            else $state.go('subjects', {sort: JSON.stringify($scope.sortParams), filter : JSON.stringify($scope.sendSearchRequest), offset:offset-limit});
        };

        $scope.goNextPage = function () {
            if (offset >= ($scope.pagination.length-1)*limit) return false;
            $state.go('subjects', {sort: JSON.stringify($scope.sortParams), filter : JSON.stringify($scope.sendSearchRequest), offset:offset+limit});
        };


        $scope.goToState = function () {
            offset = arguments.length?arguments[0]:offset;
            $state.go('subjects',{sort: JSON.stringify($scope.sortParams), filter: JSON.stringify($scope.sendSearchRequest),offset:offset});
            offset = arguments.length ? arguments[0] : offset;
            $state.go('subjects', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset
            });
        };

        $scope.setSort = function (key) {
            if ($scope.sortParams[key] >= 2 || $scope.sortParams[key] == null) {
                $scope.sortParams[key] = 0;
            } else {
                $scope.sortParams[key]++;
            }
            $scope.sendRequest();
        };
        
        ngSocket.on('scheduleList', function (result) {
            if (result.err) {
                alert(result.message);
                return false
            }
            $scope.schedules = result.data.rows;
            var countPages = Math.round(result.data.count / (limit - 1));
            $scope.pagination = [];
            for (var i = 0; i < countPages; i++) {
                $scope.pagination.push({num: i, offset: limit * i, active: ((!offset && i==0) || offset == (limit * i))});
            }
        });
    }])
});