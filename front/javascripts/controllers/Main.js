define(['./module','jquery'],function(controllers,$){
    'use strict';
    controllers.controller('Main',['$scope','$http', '$rootScope', 'ngSocket', '$sessionStorage', '$state', function($scope,$http,$rootScope,ngSocket,$sessionStorage,$state){

        $rootScope.user = {
            id: $sessionStorage.user_id,
            firstName: $sessionStorage.firstName,
            lastName: $sessionStorage.lastName,
            patronymic: $sessionStorage.patronymic
        };
        
        $scope.debug=[];

        ngSocket.emit('getUserInfo',{});

        ngSocket.on('debug', function (data) {
            $scope.debug.push({title:data.title,data:JSON.parse(data.data)});
            // console.log('debug:',data);
        });

        $scope.pageTitle = "";
        $scope.pageBreadcrumbs ="";

        $rootScope.$on('$stateChangeStart', function(event, toState){
            var breadcrumbs = toState.data.breadcrumbs;
            var title = [{'title':'Главная','state':'home'}];
            if($sessionStorage.role!=undefined){
                switch ($sessionStorage.role){
                    case 1:
                        $scope.headerButtons=true;
                        break;
                    case 2:
                        $scope.headerButtons=false;
                        break;
                    case 3:
                        $scope.headerButtons = toState.data.headerButtons;
                        break;
                    default:
                        $scope.headerButtons=true;
                        break;
                }
            }

            if(breadcrumbs!=undefined && breadcrumbs.length){
                breadcrumbs.forEach(function(i){
                    title.push(i);
                });
            }
            if(toState.data.title!=undefined && toState.data.title){
                $scope.pageTitle = toState.data.title;
            }
            $scope.pageBreadcrumbs = title;
        });

        $scope.logout = function () {
            if(confirm('Вы уверены?')){
                $http.get('api/users/logout')
                    .then(function (){
                        $sessionStorage.auth = 0;
                        $sessionStorage.role = 0;
                        $state.go('home');
                    }, function () {
                        alert('ошибка');
                    })
            }
        };
        $scope.deleteLink = null;
        $scope.deleteId = null;

        $scope.showDeleteModal = function (title, methodLink, id) {
            $('#userDelete').modal('show');
            $scope.delTitle=title;
            $scope.deleteLink = methodLink;
            $scope.deleteId = id;
        }
    }])
});