define([
    './Main',
    './Home',
    './Users',
    './Classes',
    './Records',
    './Profiles',
    './Lessons',
    './Superadmin',
    './Schedule',
    './Lesson',
    './Offices',
    './Subject'
], function () {});