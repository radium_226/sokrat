/**
 * Created by rabota on 03.06.2016.
 */
'use strict';
const Class = require('../models/').Class;

module.exports = function(socket, data) {
    if (!data.id) {
        socket.emit('deleteClass',
            {err: 1, message: 'Undefined class identifier'}
        );
        return
    }
    Class.findById(+data.id).then(
        function (result)
        {
            result.isArchive = true;

            return result.save().then(function(result){
                socket.emit('deleteClass', {
                    'err': 0,
                    class: {
                        classId : result.id,
                        className: result.className
                    }
                });
            }).catch(function (err) {
                socket.emit('deleteClass',
                    {err: 1, message: err.message}
                );
            })
        }).catch(function (err) {
        socket.emit('deleteClass',
            {err: 1, message: err.message}
        );
    })
};