/**
 * Created by Alex on 31.05.2016.
 */
'use strict';
const User = require('../models/').User;
const School = require('../models/').School;
module.exports = function (socket, data) {
    if (!data.id) {
        socket.emit('userChanged',
            {err: 1, message: 'Undefined user identifier ' + data.id}
        );
        return
    }

    if (!(data.username && data.password)) {
        socket.emit('userChanged',
            {err: 1, message: 'Incorrect username or password ' + data.username + ' ' + data.password}
        );
        return
    }

    const role = socket.request.user.roleId;
    // Todo : Использовать право createUser
    if ((!role) || (role > 2)) {
        socket.emit('userCreated', {
            'err': 1,
            message: 'no access for role = ' + role
        });
        return false;
    }

    User.findById(+data.id, {}).then(function (user) {
        user.username = data.username;
        user.email = data.email;
        user.firstName = data.firstName;
        user.lastName = data.lastName;
        user.patronymic = data.patronymic;
        user.phone = data.phone;
        user.introduce = data.introduce;
        user.implementing = data.implementing;
        user.password = data.password;
        if (+role === 1) {
            School.findById(+data.implementing.value).then(function (school) {
                school.updateAttributes({
                    numberOfClasses: +data.school.numberOfClasses
                })
            });
        }
        return user.save().then(function (user) {
            socket.emit('userChanged', {
                'err': 0,
                user: {
                    userId: user.id,
                    username: user.username
                }
            });

        }).catch(function (err) {
            socket.emit('userChanged',
                {err: 1, message: err.message}
            );
        })
    }).catch(function (err) {
        socket.emit('userChanged',
            {err: 1, message: err.message}
        );
    })
};