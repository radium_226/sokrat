/**
 * Created by emmtech on 12.08.16.
 */
'use strict';

module.exports = function (socket, passportSocketIo, io, state) {
    const Schedule = require('../models').Schedule;
    const Learner = require('../models').Learner;
    const Class = require('../models').Class;
    const sequelize = require('sequelize');
    let schedule_id;
    let user = socket.request.user;
    if (socket.handshake.query.loc) {
        if (socket.handshake.query.loc.indexOf('/lesson') + 1) {
            schedule_id = getParameterByName('id', socket.handshake.query.loc);
        }
        if (socket.handshake.query.loc.indexOf('/lesson/learner') + 1) {
            schedule_id = getParameterByName('id', socket.handshake.query.loc);
        }
        if (schedule_id) {
            Schedule.findById(+schedule_id).then((schedule)=>
                socket.join('lesson:' + schedule.id));
        } else {
            socket.join('lesson:' + schedule_id);
        }
    }

    if(socket.request.user.logged_in) {
        let usrs = passportSocketIo.filterSocketsByUser(io, function (u) {
            if(u.id !== undefined && u.id) {
                return u.id.toString() === user.id.toString()
            }
            return false;
        });
        if(!usrs.length || state === 1) {
            if(user.roleId === 3) {
                Schedule.findAll({
                    userId: user.id
                }).then((schedules)=>{
                    socket.to('auction:' + (+schedules.id)).emit('changeUserState');
                    user.state = state;
                    return user.save()
                })
            }
            if (user.roleId === 4) {
                Learner.findAll({
                    include: [{
                        model: Class,
                        attributes: ['id'],
                        include: [{
                            model: Schedule,
                            attributes: ['id']
                        }]
                    }],
                    where: {
                        userId: user.id
                    }
                }).then((learners)=> {
                    learners.forEach((learner)=>{
                        socket.to('auction:' + (+learner.scheduleId)).emit('changeUserState');
                        user.state = state;
                        if (state === 1) {
                            learner.departed = true;
                            return learner.save()
                        } else if(state === null){
                            learner.departed = false;
                            learner.save()
                        }
                        user.save()
                    });

                })
            }

        }
    }

};

function getParameterByName(name, url) {
    if (!url) return false;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}