/**
 * Created by rabota on 04.06.2016.
 */
'use strict';
const Class = require('../models/').Class;
const User = require('../models').User;
const Learner = require('../models').Learner;

module.exports = function(socket, data) {
    if (!data.id) {
        socket.emit('catchClass',
            {err: 1, message: 'Undefined class identifier'}
        );
        return
    }

    Class.findById(+data.id)
        .then(function (result) {
            Learner.findAll({
                where: {
                    classId : +data.id
                }
            }).then(function(learners) {
                var userList = [];
                for (var learner in learners) {
                    userList.push(learners[learner].userId);
                }
                User.findAll({
                    where: {
                        id: {
                            in: userList
                        }
                    }
                }).then( function (users) {
                    socket.emit('catchClass', {
                        'err': 0,
                        data: result,
                        users: users
                    });
                })
            })
        }).catch(function (err) {
        socket.emit('catchClass',
            {err: 1, message: err.message}
        );
    })
};