/**
 * Created by emmtech on 13.06.16.
 */
'use strict';

const Schedule = require('../models/').Schedule;
module.exports = function (socket, data) {

    if((!data.begin_ts)) {
        socket.emit('createSchedule', {
                err: 1,
                message: 'Incorrect date'
            }

        );
        return
    }
    const _ScheduleData = {
        begin_ts: data.begin_ts,
        time: data.time,
        lessonId: data.lessonId,
        classId: data.classId,
        officeId: data.officeId,
        duration: data.duration,
        topic: data.topic,
        userId: data.userId,
        isArchive: false,
        tag: data.tag,
        durationEnd: data.durationEnd,
        schoolId: socket.request.user.schoolId
    };
    Schedule.create(_ScheduleData).then(function () {
        socket.emit('createSchedule', {
            'err': 0
        });
    }).catch(function (err) {
        socket.emit('createSchedule', {err: 1, message: err.message});
    })
};