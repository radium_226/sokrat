/**
 * Created by rabota on 03.06.2016.
 */
'use strict';

const Class = require('../models/').Class;

module.exports = function (socket, data) {
    const _offset = data.offset ? parseInt(data.offset) : 0;
    const _limit = data.limit ? parseInt(data.limit) : null;
    let _order = [];
    let where = {
        isArchive: false,
        schoolId : +socket.request.user.schoolId
    };

    // функция сортировки по возрастанию\убыванию
    if (data.sort != undefined) {
        for (var key in data.sort) {
            if (key !== null && data.sort[key]) {
                _order.push([key, (data.sort[key] - 1 ? 'DESC' : 'ASC')])
            }
        }
    }

    if (data.filter !== undefined) {
        if (data.filter.name !== undefined && data.filter.name.trim()) {
            where.className = {
                $iLike: `%${data.filter.name}%`
            };
        }

        if (data.filter.comment !== undefined && data.filter.comment.trim()) {
            where.commentaries = {
                $iLike: `%${data.filter.comment}%`
            };
        }
    }

    Class.findAndCountAll({
        where: where,
        offset: _offset,
        limit: _limit,
        order: _order
    }).then(function (result) {
        socket.emit('classList', {
            'err': 0,
            data: result
        });
    }).catch(function (err) {
        socket.emit('classList',
            {err: 1, message: err.message}
        );
    });
};