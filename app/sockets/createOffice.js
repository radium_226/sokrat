/**
 * Created by emmtech on 08.06.16.
 */
'use strict';
var Office = require('../models/').Offices;
var Camera = require('../models/').Camera;
module.exports = function (socket, data) {

    if ((!data.number)) {
        socket.emit('createOffice', {
                err: 1,
                message: 'Введите номер кабинета'
            }
        );
        return
    }

    const _OfficeData = {
        number: data.number,
        lessonId: data.lessonId,
        webCam: (data.webCam?data.webCam:null),
        isArchive: false,
        schoolId: +socket.request.user.schoolId
    };


    Office.create(_OfficeData).then(function (office) {
        let camerasCreated = [];
        if(data.cameras.length) {
            data.cameras.forEach(function (camera) {
                if (+office.id) {
                    if(camera.camName.trim().length && camera.url_cam.trim().length){
                        let _CameraData = {
                            // cameras: data.cameras
                            isArchive: false,
                            officeId: office.id,
                            name: camera.camName,
                            url: camera.url_cam,
                            schoolId: +socket.request.user.schoolId
                        };
                        camerasCreated.push(Camera.create(_CameraData));
                    }else{
                        camerasCreated.push(
                            Camera.findOne({
                                where:{
                                    $and:[
                                        ['"camera"."officeId" IS NULL'],
                                        {id : camera.cameraId}
                                    ]
                                }
                            }).then( (camera)=>{
                                if((camera)==null){
                                    return true;
                                }
                                camera.officeId = office.id;
                                return camera.save();
                            })
                        );
                    }
                }
            });

            return Promise.all(camerasCreated).then( () => new Promise((resolve,reject) => resolve(office)));
        }else{
            return new Promise((resolve,reject) => resolve(office))
        }
    }).then(function (office) {
        socket.emit('createOffice', {
            'err': 0,
            newOffice: {
                officeId: office.id,
                number: office.number
            }
        });
    }).catch((err) => socket.emit('createOffice', {err: 1, message: err.message}));
};
