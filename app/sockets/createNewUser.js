/**
 * Created by Alex on 30.05.2016.
 */
'use strict';

const User = require('../models/').User;
const Learner = require('../models/').Learner;
const School = require('../models/').School;
module.exports = function (socket, data) {
    const role = socket.request.user.roleId;
    // Todo : Использовать право createUser
    if ((!role) || (role > 2)) {
        socket.emit('userCreated', {
            'err': 1,
            message: 'no access for role = ' + role
        });
        return false;
    }

    var _userName = data.email.split('@')[0];

    if (!((_userName) && (data.password))) {
        socket.emit('userCreated',
            {err: 1, message: 'Incorrect username or password'}
        );
        return
    }

    let _userData = {
        username: _userName,
        email: data.email,
        firstName: data.firstName,
        lastName: data.lastName,
        patronymic: data.patronymic,
        phone: data.phone,
        introduce: data.introduce,
        password: data.password,
        isArchive: false,
        roleId: data.roleId
    };
    return new Promise(function (resolve, reject) {
        //Если суперадмин создаёт
        if (+role === 1 && data.implementing !== undefined) {
            _userData.roleId = 2;
            if (+data.implementing.value) {
                //Если выбрана существующая школа
                return School.findById(+data.implementing.value).then(function (school) {
                    school.updateAttributes({
                        numberOfClasses : +data.school.numberOfClasses
                    }).then(function () {
                        resolve(school.id);
                    });
                }).catch(reject);
            } else if (data.implementing.text.trim()) {
                //Если введено название новой школы
                return School.create({
                    name: data.implementing.text.trim(),
                    numberOfClasses: +data.school.numberOfClasses,
                    isArchive: false
                }).then(function (school) {
                    resolve(school.id);
                }).catch(reject);
            } else {
                reject('params for school undefined');
            }
        } else {
            resolve(+socket.request.user.schoolId);
        }
    }).then(function (schoolId) {
        _userData.schoolId = schoolId;
        return User.create(_userData).then(function (user) {
            //Если создают ученика
            if (+data.roleId === 4) {
                const _learnerData = {
                    userId: user.id,
                    classId: data.classId,
                    isArchive: false
                };

                return Learner.create(_learnerData).then(function () {
                    emitSuccess(user)
                });
            } else {
                emitSuccess(user)
            }
        });
    }).catch(function (err) {
        emitError(err)
    });

    function emitSuccess(user) {
        socket.emit('userCreated', {
            'err': 0,
            newUser: {
                userId: user.id,
                username: user.username
            }
        });
    }

    function emitError(err) {
        socket.emit('userCreated',
            {err: 1, message: err.message}
        );
    }
};