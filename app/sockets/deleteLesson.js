/**
 * Created by rabota on 02.06.2016.
 */
var Lesson = require('../models/').Lesson;

module.exports = function(socket, data) {
    if (!data.lessonId) {
        socket.emit('lessonDeleted',
            {err: 1, message: 'Undefined lesson identifier'}
        );
        return
    }
    Lesson.findById(+data.lessonId).then(
        function (lesson)
        {
            lesson.isArchive = true;

            lesson.save().then(function(lesson){
                socket.emit('lessonDeleted', {
                    'err': 0,
                    lesson: {
                        LessonId : lesson.id,
                        lessonName: lesson.lessonName
                    }
                });
            }).catch(function (err) {
                socket.emit('lessonDeleted',
                    {err: 1, message: err.message}
                );
            })
        }).catch(function (err) {
        socket.emit('lessonDeleted',
            {err: 1, message: err.message}
        );
    })
};
        
    

