/**
 * Created by piligrim on 6/16/16.
 */
'use strict';
const Camera = require('../models/').Camera;
const Offices = require('../models/').Offices;

module.exports = function (socket, data) {
    var _limit = data.limit ? parseInt(data.limit) : null;
    let where = {
        isArchive: false,
        schoolId : +socket.request.user.schoolId
    };
    if(data.id!==undefined && +data.id){
        where.officeId=+data.id;
    }

    Camera.findAndCountAll({
        where: where,
        limit: _limit
    }).then(function (result) {
        return Offices.findById(where.officeId,{attributes:['webCam']}).then(function (office) {
            socket.emit('camerasList', {
                'err': 0,
                data: result,
                webCam: (office && office.webCam ? office.webCam :null)
            });
        });
    }).catch(function (err) {
        socket.emit('camerasList',
            {err: 1, message: err.message}
        );
    });
};