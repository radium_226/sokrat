/**
 * Created by piligrim on 6/17/16.
 */
'use strict';
const Office = require('../models/').Offices;
var Camera = require('../models/').Camera;

module.exports = function(socket, data) {
    if (!data.id) {
        socket.emit('officeChanged',
            {err: 1, message: 'Undefined user identifier'}
        );
        return
    }
    Office.findById(+data.id).then(

        (result) => {
            result.id = data.id;
            result.number = data.editedOfficeNumber;
            result.lessonId = data.editedLessonId;
            result.webCam = (data.webCam?data.webCam:null);

            return result.save().then(function (office) {

                let camerasCreated = [];
                if(data.cameras.length) {
                    data.cameras.forEach(function (camera) {
                        if (+office.id) {
                            if(camera.camName.trim().length && camera.url_cam.trim().length){
                                let _CameraData = {
                                    // cameras: data.cameras
                                    isArchive: false,
                                    officeId: office.id,
                                    name: camera.camName,
                                    url: camera.url_cam,
                                    schoolId: +socket.request.user.schoolId
                                };
                                camerasCreated.push(Camera.create(_CameraData));
                            }else{
                                camerasCreated.push(
                                    Camera.findOne({
                                        where:{
                                            $and:[
                                                ['"camera"."officeId" IS NULL'],
                                                {id : camera.cameraId}
                                            ]
                                        }
                                    }).then( (camera)=>{
                                        if((camera)==null){
                                            return true;
                                        }
                                        camera.officeId = office.id;
                                        return camera.save();
                                    })
                                );
                            }
                        }
                    });

                    return Promise.all(camerasCreated).then( () => new Promise((resolve,reject) => resolve(office)));
                }else{
                    return new Promise((resolve,reject) => resolve(office))
                }

            }).then((result)=>{
                socket.emit('officeEdited', {
                    'err': 0,
                    office: {
                        officeId: result.id,
                        officeNumber: result.number
                    }
                });
            }).catch((err) => {
                socket.emit('officeEdited',
                    {err: 1, message: err.message}
                );
            })



        }).catch(function (err) {
        socket.emit('officeEdited',
            {err: 1, message: err.message}
        );
    })
};