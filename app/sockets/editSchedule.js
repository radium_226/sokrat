/**
 * Created by emmtech on 17.06.16.
 */
'use strict';
const Schedule = require('../models/').Schedule;

module.exports = function(socket, data) {
    if (!data.id) {
        socket.emit('editSchedule',
            {err: 1, message: 'Undefined schedule identifier'}
        );
        return
    }
    Schedule.findById(+data.id).then(
        function (result)
        {
            if(data.begin_ts !== undefined && data.begin_ts) {
                result.begin_ts = data.begin_ts;
            }
            if(data.time !== undefined && data.time)
            result.time = data.time;
            if(data.lessonId !== undefined && data.lessonId) {
                result.lessonId = data.lessonId;
            }

            if(data.classId !== undefined && data.classId){
                result.classId = data.classId;
            }
            
            if(data.tag !== undefined && data.tag) {
                result.tag = data.tag;
            }
            return result.save().then(function(){
                socket.emit('editSchedule', {
                    'err': 0
                });
            }).catch(function (err) {
                socket.emit('editSchedule',
                    {err: 1, message: err.message}
                );
            })
        }).catch(function (err) {
        socket.emit('editSchedule',
            {err: 1, message: err.message}
        );
    })
};