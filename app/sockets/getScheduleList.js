/**
 * Created by emmtech on 13.06.16.
 */
'use strict';
const Class = require('../models/').Class;
const Schedule = require('../models/').Schedule;
const Lesson  =require('../models/').Lesson;
const Office = require('../models/').Offices;
const User = require('../models/').User;
const Learner = require('../models/').Learner;
const Sequelize = require('sequelize');

module.exports = function (socket, data) {
    let _offset = data.offset ? parseInt(data.offset) : 0;
    let _limit = data.limit ? parseInt(data.limit) : null;
    let _order = [];
    console.log(data.sort, _order);
    let include = [];
    let where = {
        isArchive: false
    };
    if (data.sort != undefined) {
        if (data.sort['officeNumber'] !== undefined && data.sort['officeNumber']) {
            _order.push([Office, 'number', data.sort['officeNumber'] - 1 ? 'DESC' : 'ASC']);
            data.sort['officeNumber'] = 0;
        }
        if (data.sort['className'] !== undefined && data.sort['className']) {
            _order.push([Class, 'className', data.sort['className'] - 1 ? 'DESC' : 'ASC']);
            data.sort['className'] = 0;
        }
        if (data.sort['lessonName'] !== undefined && data.sort['lessonName']) {
            _order.push([Lesson, 'lessonName', data.sort['lessonName'] - 1 ? 'DESC' : 'ASC']);
            data.sort['lessonName'] = 0;
        }
        if (data.sort['fio'] !== undefined && data.sort['fio']) {
            _order.push([User, 'lastName', data.sort['fio'] - 1 ? 'DESC' : 'ASC']);
            _order.push([User, 'firstName', data.sort['fio'] - 1 ? 'DESC' : 'ASC']);
            _order.push([User, 'patronymic', data.sort['fio'] - 1 ? 'DESC' : 'ASC']);
            data.sort['fio'] = 0;
        }
        for (var key in data.sort) {
            if (key !== null && data.sort[key]) {
                _order.push([key, (data.sort[key] - 1 ? 'DESC' : 'ASC')])
            }
        }
    }

    if (data.filter !== undefined) {

        if(data.filter.topic !== undefined && data.filter.topic.trim()) {
            where.topic = {
                $iLike: `%${data.filter.topic.trim()}%`
            }
        }
        if(data.filter.office !== undefined && +data.filter.office) {
            include.push({
                model: Office,
                where: {number: +data.filter.office},
                attributes: ['number']
            })
        } else {
            include.push({
                model: Office
            })
        }

        if(data.filter.class !== undefined && data.filter.class.trim()) {
            include.push({
                model: Class,
                where: {className: {$iLike: `%${data.filter.class.trim()}%`}},
                attributes: ['className']
            })
        } else {
            include.push({
                model: Class
            })
        }

        if(data.filter.lesson !== undefined && data.filter.lesson.trim()) {
            include.push({
                model: Lesson,
                where: {lessonName: {$iLike: `%${data.filter.lesson.trim()}%`}},
                attributes: ['lessonName']
            })
        } else {
            include.push({
                model: Lesson
            })
        }
        if(data.filter.teacher !== undefined && data.filter.teacher.trim()) {
            include.push({
                model: User,
                where: {$or: {
                    firstName: {$iLike : `%${data.filter.teacher.trim()}%`},
                    lastName: {$iLike : `%${data.filter.teacher.trim()}%`},
                    patronymic: {$iLike : `%${data.filter.teacher.trim()}%`}
                }
            },
                    attributes: ['firstName','lastName','patronymic']
            });
        } else {
            include.push({
                model: User,
                attributes: ['firstName','lastName','patronymic']
            })
        }

        if(data.filter.state !== undefined && data.filter.state.trim()) {
            let state;
            switch (data.filter.state) {
                case 'не начат':
                    state = null;
                    break;
                case 'начат':
                    state = 1;
                    break;
                case 'приостановлен':
                    state = 2;
                    break;
                case 'завершен':
                    state = 3;
                    break;
            }
            where = {state: state}
        }
        if (data.filter.time !== undefined && data.filter.time) {
            let t = data.filter.time.trim().split(':');
            if(t.length <= 2) {
                where.$and = [['"schedule"."time"::text iLike ?',(t.length  == 1 ?
                    `%${t[0].trim()}%` :
                    `%${t[0].trim()}:%${t[1].trim()}%`)],
                    ['TRUE=TRUE',[]]]
            }
            //where.time = +data.filter.time
        }

        if (data.filter.lessonId !== undefined && +data.filter.lessonId) {
            where.lessonId = +data.filter.lessonId
        }
        if (data.filter.classId !== undefined && +data.filter.classId) {
            where.classId = +data.filter.classId
        }
        if (data.filter.periodId !== undefined && +data.filter.periodId) {
            let currentDate = new Date();
            switch (data.filter.periodId){
                case 1:
                    currentDate.setDate(currentDate.getDate()-7);
                    break;
                case 2:
                    currentDate.setMonth(currentDate.getMonth()-1);
                    break;
                case 3:
                    currentDate.setMonth(currentDate.getMonth()-3);
                    break;
                case 4:
                    currentDate.setMonth(currentDate.getMonth()-6);
                    break;
                case 5:
                    currentDate.setMonth(currentDate.getMonth()-11);
                    break;

            }
            where.begin_ts = {$gte: currentDate};
        }
    }
    if (data.filter.begin_ts !== undefined && data.filter.begin_ts.trim()) {
        let d = data.filter.begin_ts.trim().split('.');
        if(d.length){
            if(d.length < 3) {
                where.$and = [['"schedule"."begin_ts"::text iLike ?',(d.length  == 1? `%${d[0].trim()}%` : `%${d[1].trim()}-%${d[0].trim()}%`)], ['TRUE=TRUE',[]]]
            } else {
                let tomorrow = new Date(`${d[2]}-${d[1]}-${+d[0]} 00:00:00.000 +00:00`);
                let yesterday = new Date(tomorrow.getTime() - 24 * 60 * 60 * 1000);
                tomorrow = new Date(tomorrow.getTime() + 24 * 60 * 60 * 1000);
                where.begin_ts = {
                    $lt: tomorrow,
                    $gt: yesterday
                };
            }
        }
    }


    Schedule.findAndCountAll({
        where: where,
        offset: _offset,
        limit: _limit,
        order: _order,
        include
    }).then(function (result) {
        socket.emit('scheduleList', {
            'err': 0,
            data: result
        });
    }).catch(function (err) {
        socket.emit('scheduleList',
            {err: 1, message: err.message}
        );
    });
};