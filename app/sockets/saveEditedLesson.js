/**
 * Created by piligrim on 03.06.16.
 */
var Lesson = require('../models/').Lesson;

module.exports = function(socket, data) {
    if (!data.lessonId) {
        socket.emit('lessonEdited',
            {err: 1, message: 'Undefined lesson identifier'}
        );
        return
    }
    Lesson.findById(+data.lessonId).then(
        function (lesson)
        {
            lesson.lessonName = data.lessonName;
            lesson.description = data.description;

            lesson.save().then(function(lesson){
                socket.emit('lessonEdited', {
                    'err': 0,
                    lesson: {
                        lessonId : lesson.id,
                        lessonName: lesson.lessonName
                    }
                });
            }).catch(function (err) {
                socket.emit('lessonEdited',
                    {err: 1, message: err.message}
                );
            })
        }).catch(function (err) {
        socket.emit('lessonEdited',
            {err: 1, message: err.message}
        );
    })
};