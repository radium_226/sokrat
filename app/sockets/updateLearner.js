/**
 * Created by emmtech on 25.06.16.
 */

const Learner = require('../models/').Learner;

module.exports = function(socket, data) {

    Learner.findOne({where:{userId: socket.request.user.id}}).then(function (result)
        {
            if(data.departed!==undefined && data.departed!==null){
                result.departed = data.departed;
            }
            if(data.armUp!==undefined && data.armUp!==null) {
                result.armUp = data.armUp;
            }
            if(data.armUp!==undefined) {
                result.scheduleId = data.scheduleId;
            }

            return result.save().then(function(){
                socket.emit('learnerUpdate', {
                    'err': 0,
                    data: result
                });
                socket.to('lesson:'+(+data.scheduleId)).emit('learnerDeparted', {
                    userId:socket.request.user.id,
                    departed:data.departed,
                    armUp: data.armUp
                });
            }).catch(function (err) {
                socket.emit('learnerUpdate',
                    {err: 1, message: err.message}
                );
            })
        }).catch(function (err) {
        socket.emit('learnerUpdate',
            {err: 1, message: err.message}
        );
    })
};
