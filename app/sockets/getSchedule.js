/**
 * Created by emmtech on 16.06.16.
 */
'use strict';
const Schedule = require('../models/').Schedule;
const Lesson = require('../models/').Lesson;
const Class = require('../models/').Class;
const User = require('../models/').User;
const Learner = require('../models/').Learner;
module.exports = function (socket, data) {
    if (!data.id) {
        socket.emit('catchSchedule',
            {err: 1, message: 'Undefined schedule identifier'}
        );
        return
    }

    Schedule.findById(+data.id, {
        include: [Lesson, Class, User]
    }).then(function (result) {

        if(+socket.request.user.roleId === 3) {
            const d = new Date(result.begin_ts);
            if(d.getTime() > Date.now()) {
                socket.emit('notStart',{});
            }
        }
        if (+socket.request.user.roleId === 4) {
            return Learner.findOne({
                where: {
                    userId: +socket.request.user.id
                },
                include: [Class, User]
            }).then(function (learner) {
                if (learner.classId !== result.classId) {
                    return socket.emit('catchSchedule',
                        {err: 1, message: 'no access'}
                    );
                }
                learner.scheduleId = result.id;
                return learner.save().then(function () {
                    emitSuccess(result);
                })
            });
        } else {
            emitSuccess(result);
        }
    }).catch(function (err) {
        emitError(err)
    });

    function emitSuccess(result) {
        socket.emit('catchSchedule', {
            'err': 0,
            data: result
        });
        socket.join('lesson:' + result.id);
    }

    function emitError(err) {
        socket.emit('catchSchedule',
            {err: 1, message: err.message}
        );
    }
};