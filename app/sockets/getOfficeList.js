/**
 * Created by emmtech on 08.06.16.
 */
'use strict';

const Office = require('../models/').Offices;
const Lesson = require('../models/').Lesson;
module.exports = function (socket, data) {
    var _offset = data.offset ? parseInt(data.offset) : 0;
    var _limit = data.limit ? parseInt(data.limit) : null;
    var _order = [];
    let include = [ Lesson ];
    var where = {
        isArchive: false,
        schoolId : +socket.request.user.schoolId
    };

    if (data.sort != undefined) {
        if (data.sort['lessonName'] !== undefined && data.sort['lessonName']) {
            _order.push([Lesson, 'lessonName', data.sort['lessonName'] - 1 ? 'DESC' : 'ASC']);
            data.sort['lessonName'] = 0;
        }
        for (var key in data.sort) {
            if (key !== null && data.sort[key]) {
                _order.push([key, (data.sort[key] - 1 ? 'DESC' : 'ASC')])
            }
        }
    }

    if(data.filter !== undefined) {
        if(data.filter.number !== undefined && +data.filter.number) {
            where.number = +data.filter.number
        }

        if(data.filter.lesson !== undefined && data.filter.lesson.trim()) {
            include = {
                model: Lesson,
                where: {lessonName: {$iLike: `%${data.filter.lesson.trim()}%`}}
            };
        }
    }

    Office.findAndCountAll({
        where: where,
        offset: _offset,
        limit: _limit,
        order: _order,
        include
    }).then(function (result) {
        socket.emit('officeList', {
            'err': 0,
            data: result
        });
    }).catch(function (err) {
        socket.emit('officeList',
            {err: 1, message: err.message}
        );
    });
};