/**
 * Created by piligrim on 29.06.16.
 */
'use strict';

const File = require('../models/').File;
const Schedule = require('../models/').Schedule;

module.exports = function (socket, data) {
    let where = {
        isArchive: false
    };
    let fileSchedule = Schedule;
    if(data.scheduleId != undefined && +data.scheduleId){
        fileSchedule = {
            model:Schedule,
            where:{
                id: +data.scheduleId
            }
        }
    }

    File.findAll({
        where,
        include:[fileSchedule]
    }).then(function (result) {
        let resp={
            'err': 0,
            files: result
        };
        socket.emit('fileList', resp);
        if(+data.scheduleId){
            socket.to('lesson:'+(+data.scheduleId)).emit('fileList', resp);
        }
    }).catch(function (err) {
        socket.emit('fileList',
            {err: 1, message: err.message}
        );
    });
};