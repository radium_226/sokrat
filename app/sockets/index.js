//Сокеты
'use strict';
module.exports = function(io,passportSocketIo) {
    io.on('connection', function(socket){
        require('./changeUserState')(socket, passportSocketIo, io, null);
        const user=socket.request.user;
        // socket.io=io;
        if(socket.request.user.logged_in){
            socket.passport=passportSocketIo;
            passportSocketIo.filterSocketsByUser(io, function(u){
                if(u._id!=undefined && u._id){
                    return u._id.toString() === user._id.toString();
                }
                return false;
            }).forEach(function(s){
                s.emit('auth', {id:user._id, socket:socket.id});
            });
        }else{
            socket.emit('auth',{id:0});
        }

        socket.on('connection', function(data){
            console.log(data);
        });

        //список событий в виде массива (указать имя файла-обработчика)
        const events=[
            //User
            {event:'getUserInfo', access:0, comment: "получение информации о пользователе" },
            {event:'createNewUser', access:0, comment: "создание нового пользователя"},
            {event:'createNewUserFromCSV', access:0, comment: "создание новых пользователей из CSV"},
            {event:'editUser', access:0, comment: "редактирование пользователя"},
            {event:'deleteUser', access:0, comment: "удаление пользователя"},
            {event:'getUserList', access:0, comment: "запрос списка пользователей"},
            {event:'getUser', access:0, comment: "получение информации пользователя"},
            {event:'getUserRoles', access:0, comment: "получение списка ролей пользователей"},
            {event:'changeUserState', access:0, comment: "востановление соединение socket"},
            //Lesson
            {event:'createLesson', access:0, comment: "создание предмета"},
            {event:'deleteLesson', access:0, comment: "удаление предмета"},
            {event:'getLessonList', access:0, comment: "получение списка предметов"},
            {event:'getLesson', access:0, comment: "получение одного предмета по id"},
            {event:'saveEditedLesson', access:0, comment: "редактирование предмета"},
            {event:'addNewFile', access:0, comment: "добавление файла"},
            {event:'getFileList', access:0, comment: "получение списка файлов"},
            {event: 'restartTeacherSteam', access:0, comment: 'обновление потока с камеры учителя'},
            //Chat
            {event:'getChatList', access:0, comment: "получение списка сообщений"},
            {event:'chat', access:0, comment: "обработка работы чата"},
            //Class
            {event:'createClass', access:0, comment: "создание класса"},
            {event:'getClassList', access:0, comment: "получение списка классов"},
            {event:'deleteClass', access:0, comment: "удаление класса"},
            {event:'editedClass', access:0, comment: "редактирование класса"},
            {event:'getClass', access:0, comment: "получение одного класса"},
            //Office
            {event:'getOfficeList', access:0, comment: "получение списка кабинетов"},
            {event:'createOffice', access:0, comment: "создание кабинета"},
            {event:'deleteOffice', access:0, comment: "удаление кабинета"},
            {event:'editOffice', access:0, comment: "редактирование кабинета"},
            {event:'getOffice', access:0, comment: "получение информации о кабинете"},
            {event:'getCamerasList', access:0, comment: "получение списка камер"},
            {event:'deleteCam', access:0, comment: "удаление камеры"},
            //Schedule
            {event:'createSchedule', access:0, comment: "создание расписания"},
            {event:'getScheduleList', access:0, comment: "запрос списка расписания"},
            {event:'getSchedule', access:0, comment: "запрос одного урока по id"},
            {event:'updateSchedule', access:0, comment: "обновление расписания"},
            {event:'deleteSchedule', access:0, comment: "удаление расписания"},
            {event:'editSchedule', access:0, comment: "редактирование расписания"},
            //Subject
            {event: 'getSubjectList', access:0, comment: 'получение списка уроков'},
            //Learner
            {event: 'getLearnersList', access:0, comment: 'получение списка учеников'},
            {event: 'updateLearner', access:0, comment: 'обновление списка учеников'},
            //Record
            {event: 'getRecordList', access:0, comment: 'получение списка записей'},
            {event: 'getRecord', access:0, comment: 'получение одной записи по id'}
        ];

        //цикл проходит по всем элементам массива events возвращая нужное событие
        events.forEach(function(val){
            socket.on(val.event, function(data){
                if(process.env.NODE_ENV === 'development'){
                    socket.emit('debug',{title:val.comment,data:JSON.stringify(data)});                    
                }
                if(!val.access || socket.request.user.logged_in) {
                    require(`./${val.event}`)(socket, data);
                }
            });
        });
        
        socket.on('callLearner', function (data) {
            passportSocketIo.filterSocketsByUser(io, function(ioUser){
                if(ioUser.id!=undefined && ioUser.id){
                    return ioUser.id.toString() === data.id.toString();
                }
                return false;
            }).forEach(function(s){
                s.emit('callFromTeacher', {teacherId:user.id});
            });
        });
        
        socket.on('callTeacher', function (data) {
            passportSocketIo.filterSocketsByUser(io, function(ioUser){
                if(ioUser.id!=undefined && ioUser.id){
                    return ioUser.id.toString() === data.teacherId.toString();
                }
                return false;
            }).forEach(function(s){
                s.emit('videoFromLearner', {name:data.name});
            });
        });


        socket.on('callTimer', function (data) {
            passportSocketIo.filterSocketsByUser(io, function(ioUser){
                if(ioUser.id!=undefined && ioUser.id){
                    return ioUser.id.toString() === data.id.toString();
                }
                return false;
            }).forEach(function(s){
                s.emit('callTimerFromTeacher', {date: data.date});
            });
        });
        socket.on('stopTimer', function (data) {
            passportSocketIo.filterSocketsByUser(io, function(ioUser){
                if(ioUser.id!=undefined && ioUser.id){
                    return ioUser.id.toString() === data.id.toString();
                }
                return false;
            }).forEach(function(s){
                s.emit('stopTimer',{});
            });
        });

        socket.on('disconnect', function () {
            require('./changeUserState')(socket, passportSocketIo, io, 1);
            console.log('dsayfsdr4');
        });
    });
};
