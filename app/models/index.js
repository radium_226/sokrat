'use strict';

var _models = {};

var SchoolInit = require('./schoolInit');
var _schoolInit = new SchoolInit(_models);
_schoolInit.fillExportModels(_models);

_schoolInit.execute();

module.exports = _models;
