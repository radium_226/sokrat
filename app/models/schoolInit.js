/**
 * Created by Alex on 07.06.2016.
 */

'use strict';

var SchoolMeta = require('./schoolInit/School'),
    OfficeMeta = require('./schoolInit/Office'),
    ClassMeta = require('./schoolInit/Classes'),
    SubjectMeta = require('./schoolInit/Subject'),
    ScheduleMeta = require('./schoolInit/Schedule'),
    LessonMeta = require('./schoolInit/Lesson'),
    RecordMeta = require('./schoolInit/Record'),
    ChatMeta = require('./schoolInit/Chat'),
    LearnerMeta = require('./schoolInit/Learner'),
    FileMeta = require('./schoolInit/File'),
    FileScheduleMeta = require('./schoolInit/FileSchedule'),
    UserMeta = require('./userInit/User.js'),
    RoleMeta = require('./userInit/Role'),
    RightMeta = require('./userInit/Right'),
    connection = require('../sequelize.js');

class Initiator {
    execute() {
        var that = this;
        return that._syncModels().then(function () {
            return that._addRoles()
        }).then(function () {
            return that._addSuperAdmin()
        }).then(function () {
            return that._addRights()
        }).then(function () {
            return that._grantRights()
        }).then(function () {
            return {
                Right: that.Right,
                Role: that.Role,
                RoleRight: that.RoleRight,
                User: that.User
            }
        }).catch(function (err) {
            console.error(err.message)
        });
    }

    constructor(models) {
        this.Right = connection.define('right', RightMeta.attributes);
        this.Role = connection.define('role', RoleMeta.attributes, RoleMeta.options);
        this.RoleRight = connection.define('role_right', {});
        this.User = connection.define('users', UserMeta.attributes, UserMeta.options);
        this.FileSchedule = connection.define('file_schedule', FileScheduleMeta.attributes);

        this.User.belongsTo(this.Role);
        this.Role.belongsToMany(this.Right, {through: this.RoleRight});
        this.Right.belongsToMany(this.Role, {through: this.RoleRight});

        this.roles = {};
        this.rights = {};
        
        this.School = connection.define('school', SchoolMeta.attributes);
        this.Class = connection.define('class', ClassMeta.attributes);
        this.Offices = connection.define('office', OfficeMeta.attributes);
        this.Camera = connection.define('camera', OfficeMeta.Camera.attributes);
        this.Video = connection.define('video', OfficeMeta.Video.attributes);
        this.Subject = connection.define('subject', SubjectMeta.attributes);
        this.Schedule = connection.define('schedule', ScheduleMeta.attributes);
        this.Lesson = connection.define('lesson', LessonMeta.attributes);
        this.Record = connection.define('record', RecordMeta.attributes, RecordMeta.options);
        this.Chat = connection.define('chat', ChatMeta.attributes);
        this.Learner = connection.define('learner', LearnerMeta.attributes);
        this.File = connection.define('file', FileMeta.attributes);


        this.File.belongsToMany(this.Schedule, {through: this.FileSchedule});
        this.Schedule.belongsToMany(this.File, {through: this.FileSchedule});

        this.Lesson.belongsTo(this.School);
        this.School.hasMany(this.Lesson);
        this.Class.belongsTo(this.School);
        this.School.hasMany(this.Class);
        this.Offices.belongsTo(this.School);
        this.School.hasMany(this.Offices);
        this.User.belongsTo(this.School);
        this.School.hasMany(this.User);
        this.Camera.belongsTo(this.School);
        this.School.hasMany(this.Camera);
        this.Offices.hasMany(this.Camera);
        this.Schedule.belongsTo(this.Class);
        this.Class.hasMany(this.Schedule);
        this.Schedule.belongsTo(this.Subject);
        this.Subject.hasMany(this.Schedule);
        this.Schedule.belongsTo(this.Lesson);
        this.Lesson.hasMany(this.Schedule);
        this.Schedule.belongsTo(this.Offices);
        this.Offices.hasMany(this.Schedule);
        this.Schedule.belongsTo(this.User);
        this.User.hasMany(this.Schedule);

        this.Learner.belongsTo(this.User);
        this.User.hasOne(this.Learner);
        this.Learner.belongsTo(this.Schedule);
        this.Learner.belongsTo(this.Class);
        this.Class.hasMany(this.Learner);

        this.Lesson.hasMany(this.Class);
        this.Offices.belongsTo(this.Lesson);
        this.Lesson.hasMany(this.Offices);
        this.Subject.belongsTo(this.Lesson);
        this.Lesson.hasMany(this.Subject);
        
        this.Record.belongsTo(this.Lesson);
        this.Record.belongsTo(this.Class);
        this.Record.belongsTo(this.Offices);
        this.Record.belongsTo(this.User);
        this.Record.hasMany(this.Video);
        this.Record.belongsTo(this.Schedule);
        this.Video.belongsTo(this.Record);
        this.Video.belongsTo(this.Camera);
        
        this.Chat.belongsTo(this.User);
        this.User.hasMany(this.Chat);
        this.Chat.belongsTo(this.Schedule);
        this.Schedule.hasMany(this.Chat);

        this.Video.belongsTo(this.Schedule);
        this.Schedule.hasMany(this.Video);
    }

    fillExportModels(models) {
        models.School = this.School;
        models.User = this.User;
        models.Role = this.Role;
        models.Right = this.Right;
        models.Camera = this.Camera;
        models.Video = this.Video;
        models.Lesson = this.Lesson;
        models.Class = this.Class;
        models.Offices = this.Offices;
        models.Schedule = this.Schedule;
        models.Subject = this.Subject;
        models.Record = this.Record;
        models.Chat = this.Chat;
        models.Learner = this.Learner;
        models.File = this.File;
        models.FileSchedule = this.FileSchedule;
    }

    _syncModels() {
        var that = this;
        return this.School.sync({force: false}).then(function () {
            return that.Role.sync({force: false})
        }).then(function () {
            return that.User.sync({force: false})
        }).then(function () {
            return that.Right.sync({force: false})
        }).then(function () {
            return that.RoleRight.sync({force: false})
        }).then(function () {
            return that.Lesson.sync({force: false})
        }).then(function () {
            return that.Offices.sync({force: false})
        }).then(function () {
            return that.Camera.sync({force: false})
        }).then(function () {
            return that.Subject.sync({force: false})
        }).then(function () {
            return that.Class.sync({force: false})
        }).then(function () {
            return that.Schedule.sync({force: false})
        }).then(function () {
            return that.Record.sync({force: false})
        }).then(function () {
            return that.Chat.sync({force: false})
        }).then(function () {
            return that.Learner.sync({force: false})
        }).then(function () {
            return that.File.sync({force: false})
        }).then(function () {
            return that.FileSchedule.sync({force: false})
        }).then(function () {
            return that.Video.sync({force: false})
        })
    }

    _addRoles() {
        var that = this;

        return that.Role.findOrCreate({
            where: {roleName: 'superadmin'},
            defaults: {
                roleName: 'superadmin',
                description: 'Суперадмин',
                isArchive: false
            }
        }).spread(function (role) {
            that.roles.sa = role;
            return that.Role.findOrCreate({
                where: {roleName: 'admin'},
                defaults: {description: 'Админ', isArchive: false}
            }).spread(function (role) {
                that.roles.admin = role;
                return that.Role.findOrCreate({
                    where: {roleName: 'teacher'},
                    defaults: {description: 'Учитель', isArchive: false}
                }).spread(function (role) {
                    that.roles.teacher = role;
                    return that.Role.findOrCreate({
                        where: {roleName: 'learner'},
                        defaults: {description: 'Ученик', isArchive: false}
                    }).spread(function (role) {
                        that.roles.learner = role;
                        return
                    })
                })
            })
        })
    }

    _addSuperAdmin() {
        var that = this;

        return that.User.findOrCreate({
            where: {username: 'sa'},
            defaults: {
                username: 'sa',
                password: 'system',
                isArchive: false
            }
        }).spread(
            function (user) {
                return user.setRole(that.roles.sa);
            }
        );
    }

    _addRights() {
        var that = this;

        return that.Right.findOrCreate({
            where: {name: 'createUser'},
            defaults: {
                name: 'createUser',
                description: 'создание пользовталея'
            }
        }).spread(function (right) {
            that.rights.createUser = right;
            return
        })
    }

    _grantRights() {
        var that = this;
        return new Promise(function (resolve) {
                that.rights.createUser.addRoles([that.roles.sa, that.roles.admin]);
                resolve()
            }
        )
    }
}

module.exports = Initiator;