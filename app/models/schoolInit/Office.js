/**
 * Created by Alex on 06.06.2016.
 */
'use strict';
var Sequelize = require('sequelize');

const attributes = {
    number: {
        type: Sequelize.INTEGER
    },
    webCam: {
        type: Sequelize.STRING
    },
    isArchive: {
        type: Sequelize.BOOLEAN
    }
};

const Camera = {
    attributes: {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        recPID: {
            type: Sequelize.INTEGER
        },
        url: {
            type: Sequelize.STRING
        },
        isEnable: {
            type: Sequelize.BOOLEAN
        },
        isArchive: {
            type: Sequelize.BOOLEAN
        }
    }
};

const Video = {
    attributes: {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        isArchive: {
            type: Sequelize.BOOLEAN
        },
        ts: {
            type: Sequelize.BOOLEAN
        }
    }
};

module.exports.attributes = attributes;
module.exports.Camera = Camera;
module.exports.Video = Video;