/**
 * Created by piligrim on 29.06.16.
 */
'use strict';
const Sequelize = require('sequelize');

const attributes = {
    isArchive: {
        type: Sequelize.BOOLEAN
    }
};

module.exports.attributes = attributes;