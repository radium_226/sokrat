/**
 * Created by emmtech on 18.06.16.
 */
'use strict';
var Sequelize = require('sequelize');
var attributes = {
    armUp: {
        type: Sequelize.BOOLEAN
    },
    isArchive: {
        type: Sequelize.BOOLEAN
    },
    departed: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    avatarName: {
        type: Sequelize.STRING
    }
};

module.exports.attributes = attributes;