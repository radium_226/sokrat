/**
 * Created by rabota on 03.06.2016.
 */
'use strict';
const Sequelize = require('sequelize');

const attributes = {
    className: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    commentaries: {
        type: Sequelize.STRING
    },
    isArchive: {
        type: Sequelize.BOOLEAN
    }
};

module.exports.attributes = attributes;
    