/**
 * Created by Alex on 07.06.2016.
 */
'use strict';
var Sequelize = require('sequelize');

var attributes = {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    description: {
        type: Sequelize.STRING
    },
    numberOfClasses: {
        type: Sequelize.INTEGER
    },
    isArchive: {
        type: Sequelize.BOOLEAN
    }
};

module.exports.attributes = attributes;