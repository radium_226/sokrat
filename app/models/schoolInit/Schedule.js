/**
 * Created by Alex on 06.06.2016.
 */
'use strict';
var Sequelize = require('sequelize');

var attributes = {
    // subject_id: {
    //     type: Sequelize.INTEGER
    // },
    // class_id: {
    //     type: Sequelize.INTEGER
    // },
    begin_ts : {
        type : Sequelize.DATEONLY
    },
    time: {
        type: Sequelize.TIME
    },
    state: {
        type: Sequelize.INTEGER
    },
    topic: {
        type: Sequelize.STRING
    },
    duration: {
        type: Sequelize.INTEGER
    },
    isArchive: {
        type: Sequelize.BOOLEAN
    },
    tag: {
        type: Sequelize.STRING
    },
    durationEnd: {
        type: Sequelize.TIME
    }
};

module.exports.attributes = attributes;