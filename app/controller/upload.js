/**
 * Created by piligrim on 22.06.16.
 */
'use strict';
const express    = require('express');
const fs         = require("fs");
const multiparty = require('multiparty');
const Learner = require('../models/').Learner;
const File = require('../models/').File;
const FileSchedule= require('../models/').FileSchedule;
// const router = express.Router();


/** обработка загруженной аватарки и сохранение ее на диск
* multiparty
*https://www.npmjs.com/package/multiparty*/

exports.avatar = function (req, res, next) {
    const userId = +req.user.id;
    // создаем форму
    var form = new multiparty.Form();
    //здесь будет храниться путь с загружаемому файлу, его тип и размер
    var uploadFile = {uploadPath: '', type: '', size: 0};
    //максимальный размер файла
    var maxSize = 2 * 1024 * 1024; //2MB
    //поддерживаемые типы(в данном случае это картинки формата jpeg,jpg и png)
    var supportMimeTypes = ['image/jpg', 'image/jpeg', 'image/png'];
    //массив с ошибками произошедшими в ходе загрузки файла
    var errors = [];

    //если произошла ошибка
    // form.on('error', function(err){
    //         if(fs.existsSync(uploadFile.path)) {
    //                 //если загружаемый файл существует удаляем его
    //                 fs.unlinkSync(uploadFile.path);
    //                 console.log('error');
    //         }
    // });

    // form.on('close', function() {
    //         //если нет ошибок и все хорошо
    //         if(errors.length == 0) {
    //                 //сообщаем что все хорошо
    //                 res.send({status: 'ok', text: 'Success'});
    //         }
    //         else {
    //                 if(fs.existsSync(uploadFile.path)) {
    //                         //если загружаемый файл существует удаляем его
    //                         fs.unlinkSync(uploadFile.path);
    //                 }
    //                 //сообщаем что все плохо и какие произошли ошибки
    //                 res.send({status: 'bad', errors: errors});
    //         }
    // });

    // при поступление файла
    form.on('part', function (part) {
        //читаем его размер в байтах
        uploadFile.size = part.byteCount;
        //читаем его тип
        uploadFile.type = part.headers['content-type'];
        //путь для сохранения файла
        var fileName = Date.now() + '.jpg';
        uploadFile.path = './front/images/upload/avatars/' + fileName;

        // //проверяем размер файла, он не должен быть больше максимального размера
        // if(uploadFile.size > maxSize) {
        //         errors.push('File size is ' + uploadFile.size + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
        // }
        //
        // //проверяем является ли тип поддерживаемым
        // if(supportMimeTypes.indexOf(uploadFile.type) == -1) {
        //         errors.push('Unsupported mimetype ' + uploadFile.type);
        // }

        //если нет ошибок то создаем поток для записи файла
        if (errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.path);
            Learner.findOne({
                where:{userId}
            }).then(
                function (result) {
                    result.avatarName = fileName;
                    result.save();
                }
            );
            part.pipe(out);
        }
        // else {
        //         //пропускаем
        //         //вообще здесь нужно как-то остановить загрузку и перейти к onclose
        //         part.resume();
        // }

    });

    // парсим форму
    form.parse(req);

    // добавляем имя аватарки в таблицу learners
    // module.exports = function(socket, data) {
    //     if(!fileName){
    //         return false;
    //     }

    // };
    
    // console.log(res.filename);
    // res.status(200).send("pong!");
};

// http://csv.adaltas.com/parse/examples/

exports.userCSV = function (req, res) {

    var csv = require('csv-parser')
    var fs = require('fs')

    var form = new multiparty.Form();

    var data = [];
    // var uploadFile = {uploadPath: '', type: '', size: 0};

    form.on('part', function (part) {
        // uploadFile.size = part.byteCount;
        // console.log('ФАЙЛ:', uploadFile);
        // res.status(200).send("pong!");
        part.pipe(csv(['username', 'email', 'firstName', 'lastName', 'patronymic', 'password', 'introduce', 'phone'],{
            raw: true,
            separator: ','
        }))
            .on('data', function(row) {
                data.push(row);
            })
            .on('end', function (){
                res.json(data);
            });
        // res.end(data);
    });
    form.parse(req);

//     var parse = require('csv-parse');
//     require('should');
//
//     var output = [];
// // Create the parser
//     var parser = parse({delimiter: ':'});
// // Use the writable stream api
//     parser.on('readable', function(){
//         var record;
//         while (record = parser.read()) {
//             output.push(record);
//         }
//     });
// // Catch any error
//     parser.on('error', function(err){
//         console.log(err.message);
//     });
// // When we are done, test that the parsed output matched what expected
//     parser.on('finish', function(){
//         output.should.eql([
//             [ 'root','x','0','0','root','/root','/bin/bash' ],
//             [ 'someone','x','1022','1022','a funny cat','/home/someone','/bin/bash' ]
//         ]);
//     });
// // Now that setup is done, write data to the stream
//     parser.write("root:x:0:0:root:/root:/bin/bash\n");
//     parser.write("someone:x:1022:1022:a funny cat:/home/someone:/bin/bash\n");
// // Close the readable stream
//     parser.end();
};

/** обработка загрузки файла и сохранение его на диск
 * multiparty
 *https://www.npmjs.com/package/multiparty*/

exports.uploadFile = function (req, res, next) {
    const userId = +req.user.id;
    // создаем форму
    var form = new multiparty.Form();
    //здесь будет храниться путь с загружаемому файлу, его тип и размер
    var uploadFile = {uploadPath: '', type: '', size: 0};
    //максимальный размер файла
    var maxSize = 2 * 1024 * 1024; //2MB
    //поддерживаемые типы(в данном случае это картинки формата jpeg,jpg и png)
    // var supportMimeTypes = ['image/jpg', 'image/jpeg', 'image/png'];
    //массив с ошибками произошедшими в ходе загрузки файла
    var errors = [];

    //если произошла ошибка
    // form.on('error', function(err){
    //         if(fs.existsSync(uploadFile.path)) {
    //                 //если загружаемый файл существует удаляем его
    //                 fs.unlinkSync(uploadFile.path);
    //                 console.log('error');
    //         }
    // });

    // form.on('close', function() {
    //         //если нет ошибок и все хорошо
    //         if(errors.length == 0) {
    //                 //сообщаем что все хорошо
    //                 res.send({status: 'ok', text: 'Success'});
    //         }
    //         else {
    //                 if(fs.existsSync(uploadFile.path)) {
    //                         //если загружаемый файл существует удаляем его
    //                         fs.unlinkSync(uploadFile.path);
    //                 }
    //                 //сообщаем что все плохо и какие произошли ошибки
    //                 res.send({status: 'bad', errors: errors});
    //         }
    // });

    // при поступление файла
    form.on('part', function (part) {

        //читаем его размер в байтах
        // uploadFile.size = part.byteCount;
        //читаем его тип
        // uploadFile.type = part.headers['content-type'];
        //путь для сохранения файла
        // var fileName = Date.now() + '_' + part.filename;
        // парсим имя файла и выделяем расширения файла
        var tags = part.filename;
        // var re = /\s*.\s*/;
        var tagList = tags.split('.');
        var newFileName = Date.now() + '.' + tagList.pop();
        console.log(tagList);
        uploadFile.path = './front/images/upload/files/' + newFileName;

        // //проверяем размер файла, он не должен быть больше максимального размера
        // if(uploadFile.size > maxSize) {
        //         errors.push('File size is ' + uploadFile.size + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
        // }
        //
        // //проверяем является ли тип поддерживаемым
        // if(supportMimeTypes.indexOf(uploadFile.type) == -1) {
        //         errors.push('Unsupported mimetype ' + uploadFile.type);
        // }

        //если нет ошибок то создаем поток для записи файла
        if (errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.path);
            
            File.create({
                fileName: newFileName,
                sourceFileName: part.filename,
                uploadedBy: +req.user.id,
                isArchive: false
            }).then(function (file) {
                FileSchedule.create({
                    scheduleId: req.query.scheduleId,
                    fileId: file.id,
                    isArchive: false
                });
            });

            part.pipe(out);
        }
        // form.on('end', function () {
        //
        // });
    });

    // парсим форму
    form.parse(req);
    form.on('close', ()=> res.json({err:0}));
    // добавляем имя аватарки в таблицу learners
    // module.exports = function(socket, data) {
    //     if(!fileName){
    //         return false;
    //     }

    // };

    // console.log(res.filename);
    // res.status(200).send("pong!");
};